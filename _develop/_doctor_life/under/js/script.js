window.addEventListener('DOMContentLoaded',function(){
  $('.rel_wrap').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: true,
    autoplaySpeed: 6000,
    dots: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          arrows: false,
          dots: false,
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 476,
        settings: {
          arrows: false,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  })

  /* Arcording */
  $('.under_c_tag ul').hide();
  $('#tags').on('click', function(e) {
    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
      $('.under_c_tag ul').slideDown();
    } else {
      $(this).removeClass('active');
      $('.under_c_tag ul').slideUp();
    }
  });
})
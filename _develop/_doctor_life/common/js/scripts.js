'use strict';

window.addEventListener('DOMContentLoaded', function() {
  new Slider();
});
var Slider = function() {
  function Slider() {
    var s = this;
    this.target = 'slider';
    this.header = document.getElementById('header');
    this.max_height_target;
    this.step = 0.00035;
    this.scale = 1.15;
    this.currentSlide = 0;
    this.opacity = 0;
    this.timer;
    this.timeout;
    this.img;
    this.eles = document.getElementById(this.target).querySelectorAll('.main_slider picture');
    this.func_transition = function() {
      s.img = s.eles[s.currentSlide];
      closest(s.img, '.slide').style.opacity = 1;
      s.scale -= s.step;
      s.img.style.transform = 'scale(' + s.scale + ')';
      s.img.style.MozTransform = 'scale(' + s.scale + ')';
      s.img.style.WebkitTransform = 'scale(' + s.scale + ')';
      if (s.scale <= 1) {
        s.currentSlide += 1;
        closest(s.img, '.slide').style.opacity = 0;
        s.timeout = setTimeout(function() {
          s.img.style.transform = 'scale(1.15)';
          s.img.style.MozTransform = 'scale(1.15)';
          s.img.style.WebkitTransform = 'scale(1.15)';
        }, 2000);
        s.scale = 1.15;
        if (s.currentSlide > s.eles.length - 1) {
          s.currentSlide = 0;
        }
      }
      s.timer = window.requestAnimFrame(s.func_transition);
    };
    this.sizeWindow = function() {
      Array.prototype.forEach.call(s.eles, function(el, i) {
        closest(el, '.slide').style.opacity = s.opacity;
        var img = new Image();
        img.onload = function() {
          s.getRatio(img);
        };
        img.src = el.getAttribute('src');
      });
      document.getElementById(s.target).style.height = s.max_height_target;
    };
    this.getRatio = function(img) {
      var ratio;
      ratio = img.naturalWidth / img.naturalHeight;
      return ratio;
    };
    window.addEventListener('resize', function() {
      s.sizeWindow();
    });
    window.addEventListener('load', function() {
      s.sizeWindow();
    });
    this.sizeWindow();
    this.func_transition();
  }
  return Slider;
}();
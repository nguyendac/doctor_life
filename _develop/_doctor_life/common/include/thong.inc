<div class="main_slider">
  <div id="slider" class="slider">
    <div class="main_slider">
      <div class="wrap">
        <div class="slide">
          <picture>
            <source media="(max-width: 768px)" srcset="/images/slider_01_sp.jpg" />
            <img src="/images/slider_01_pc.jpg" alt="Slider 01" />
          </picture>
          <span class="pos_01">医師の人生を、豊かに。</span>
        </div>
        <div class="slide">
          <picture>
            <source media="(max-width: 768px)" srcset="/images/slider_02_sp.jpg" />
            <img src="/images/slider_02_pc.jpg" alt="Slider 01" />
          </picture>
          <span class="pos_02">医師の人生を、豊かに。</span>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/.main_slider-->
<section class="st_dt_life" id="st_dt_life">
  <div class="row">
    <h2 class="ttl_section">
      ドクターズライフについて
      <span>DOCTOR’S LIFE</span>
    </h2>
    <div class="bx_about">
      <div class="bx_about_l">
        <figure>
          <img src="/images/img_top.jpg" alt="images top">
          <figcaption>
            <span>このサロンは、「医師の人生を豊かにする」を目的に設立された、医師・歯科医限定コミュニティです。私自身も医師であるがゆえに日々感じていることですが医師・歯科医師は、目の前の患者のことを優先し大切にする一方で、自分自身の生活を後回しにしてしまいがちです。</span>
            <span>そこで、ドクターズライフでは、「住居」「食事」「ファッション」「旅行」「資産運用」「キャリアアップ」などをテーマに、医師・歯科医同士が語り合い、有益な情報を交換し合う場を作りました。参加メンバー同士切磋琢磨し、人生や仕事を豊かにしていきましょう。</span>
          </figcaption>
        </figure>
      </div>
      <!--/.left-->
      <figure class="bx_about_r">
        <img src="/images/fg_doctor.png" alt="Detail sitemaps">
      </figure>
      <!--/.bx_about-->
    </div>
  </div>
</section>
<!--/.st_dt_life-->
<div class="bx_qol">
  <div class="row">
    <div class="bx_qol_b">
      <h3>医師のQOL向上に貢献</h3>
      <p>仕事やプライベートでの悩み相談や有益な情報提供を日々行っています</p>
    </div>
  </div>
</div>
<!--/.bx_qol-->
<div class="bx_list_fun">
  <div class="row">
    <ul>
      <li>
        <figure>
          <img src="/images/icon_01.png" alt="Icon 01">
          <figcaption>住居</figcaption>
        </figure>
        <em>優良物件の優先的なご紹介。さらに有名家具デザイナーによるインテリアのトータルコーディネートを提供</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_02.png" alt="Icon 02">
          <figcaption>食事</figcaption>
        </figure>
        <em>有名レストランでのグルメオフ会を実施</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_03.png" alt="Icon 03">
          <figcaption>投資</figcaption>
        </figure>
        <em>金融庁登録された現役医師投資家が実践し信頼できる資産運用・投資情報のご紹介</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_04.png" alt="Icon 04">
          <figcaption>車</figcaption>
        </figure>
        <em>国内最大手オークションから最安価で希望車種を購入、車に関わるフォローとサービス提供</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_05.png" alt="Icon 05">
          <figcaption>旅行</figcaption>
        </figure>
        <em>旅行チケットや有名ホテルの予約をメール一本で全て代行するコンシェルジュサービス</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_06.png" alt="Icon 06">
          <figcaption>メディア</figcaption>
        </figure>
        <em>出版やテレビなどの大手メディアへの露出サポート</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_07.png" alt="Icon 07">
          <figcaption>教養</figcaption>
        </figure>
        <em>自己啓発、ベストセラー作家や有名人による講演</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_08.png" alt="Icon 08">
          <figcaption>出張パーティー</figcaption>
        </figure>
        <em>出張シェフによるホームパーティープロデュース</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_09.png" alt="Icon 09">
          <figcaption>美容室</figcaption>
        </figure>
        <em>有名美容室などへの優先予約もしくは出張ヘアカット</em>
      </li>
      <li>
        <figure>
          <img src="/images/icon_10.png" alt="Icon 10">
          <figcaption>フィットネス・ダイエット</figcaption>
        </figure>
        <em>有名パーソナルジムによる健康管理サポート</em>
      </li>
    </ul>
  </div>
</div>
<!--/.bx_list_fun-->
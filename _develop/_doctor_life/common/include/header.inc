<div class="hd_box">
  <div class="hd_box_l">
    <h1 class="hd_box_l_logo"><a href="#"><img src="/common/images/logo.png" alt="Logo"></a></h1>
    <nav class="hd_box_l_nav" id="nav">
      <ul>
        <li><a href="#">ドクターズライフについて</a></li>
        <li><a href="#">会員について</a></li>
        <li><a href="#">サービス内容</a></li>
        <li><a href="#">入会条件・注意事項</a></li>
        <li><a href="#">お問い合わせ</a></li>
      </ul>
    </nav>
    <!--/.nav-->
  </div>
  <!--/.left-->
  <div class="hd_box_r">
    <a class="hd_box_r_link show_pc" id="id_reg" href="#">会員登録</a>
    <div id="humberger" class="humberger show_sp">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <!--/.humberger-->
  </div>
  <!--/.right-->
</div>
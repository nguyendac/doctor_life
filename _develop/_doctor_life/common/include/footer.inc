<div class="ft_bx" id="ft_bx">
  <div class="row">
    <figure>
      <img src="/common/images/vt_support.png" alt="vector Support">
      <figcaption>コミュニティの概要や会員についての<br class="show_pc">お問い合わせはこちらから</figcaption>
    </figure>
    <a class="ft_bx_tel" href="tel:000-0000-0000"><span>Tel</span>000-0000-0000</a>
    <a class="ft_bx_link" href="#">会員登録</a>
  </div>
</div>
<!--/.ft_bx-->
<div class="ft_top">
  <div class="row">
    <a href="#">
      <p>医師の人生を、豊かに。</p>
      <div class="ft_top_rg">
        <h3>Doctor’s Life<span>会員登録</span></h3>
      </div>
      <!--/.register-->
    </a>
  </div>
</div>
<!--/.ft_top-->
<div class="ft_bottom">
  <div class="row">
    <div class="ft_bottom_f">
      <h2 class="logo_ft"><a href=""><img src="/common/images/logo.png" alt="Logo"></a></h2>
      <div class="ft_bottom_f_up" id="gotop"><a href="/"><img src="/common/images/arr_up.png" alt="Page top"></a></div>
    </div>
    <div class="ft_bottom_s">
      <ul class="ft_bottom_s_navft">
        <li><a href="#">ドクターズライフについて</a></li>
        <li><a href="#">会員について</a></li>
        <li><a href="#">サービス内容</a></li>
        <li><a href="#">入会条件・注意事項</a></li>
        <li><a href="#">お問い合わせ</a></li>
      </ul>
      <!--/.navft-->
      <div class="ft_bottom_s_social">
        <a href="#"><img src="/common/images/icon_fb.png" alt="Icon Facebook"></a>
      </div>
      <!--/.social-->
    </div>
  </div>
</div>
<!--/.ft_bottom-->
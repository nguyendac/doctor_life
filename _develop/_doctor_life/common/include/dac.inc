<div class="bx_qol activity">
  <div class="row">
    <div class="bx_qol_b">
      <h3>活動記録</h3>
      <p>ドクターズライフで活動したみなさんのストーリーをご紹介します</p>
    </div>
  </div>
</div>
<div class="rel">
  <div class="row">
    <div class="rel_wrap">
      <article class="rel_ar">
        <figure>
          <a href=""><img src="images/under_01.jpg" alt="rel 01"></a>
          <figcaption><span>#カテゴリ</span></figcaption>
        </figure>
        <time datetime="2018-09-10">20118.09.10</time>
        <h4><a href="">タイトルが入りますタイトルが入りますタイトルが入ります</a></h4>
        <div class="sns">
          <figure><img src="images/sns.png" alt="sns"></figure>
        </div>
      </article>
      <article class="rel_ar">
        <figure>
          <a href=""><img src="images/under_02.jpg" alt="rel 02"></a>
          <figcaption><span>#カテゴリ</span></figcaption>
        </figure>
        <time datetime="2018-09-10">20118.09.10</time>
        <h4><a href="">タイトルが入りますタイトルが入りますタイトルが入ります</a></h4>
        <div class="sns">
          <figure><img src="images/sns.png" alt="sns"></figure>
        </div>
      </article>
      <article class="rel_ar">
        <figure>
          <a href=""><img src="images/under_03.jpg" alt="rel 03"></a>
          <figcaption><span>#カテゴリ</span></figcaption>
        </figure>
        <time datetime="2018-09-10">20118.09.10</time>
        <h4><a href="">タイトルが入りますタイトルが入りますタイトルが入ります</a></h4>
        <div class="sns">
          <figure><img src="images/sns.png" alt="sns"></figure>
        </div>
      </article>
      <article class="rel_ar">
        <figure>
          <a href=""><img src="images/under_04.jpg" alt="rel 04"></a>
          <figcaption><span>#カテゴリ</span></figcaption>
        </figure>
        <time datetime="2018-09-10">20118.09.10</time>
        <h4><a href="">タイトルが入りますタイトルが入りますタイトルが入ります</a></h4>
        <div class="sns">
          <figure><img src="images/sns.png" alt="sns"></figure>
        </div>
      </article>
      <article class="rel_ar">
        <figure>
          <a href=""><img src="images/under_05.jpg" alt="rel 01"></a>
          <figcaption><span>#カテゴリ</span></figcaption>
        </figure>
        <time datetime="2018-09-10">20118.09.10</time>
        <h4><a href="">タイトルが入りますタイトルが入りますタイトルが入ります</a></h4>
        <div class="sns">
          <figure><img src="images/sns.png" alt="sns"></figure>
        </div>
      </article>
      <article class="rel_ar">
        <figure>
          <a href=""><img src="images/under_06.jpg" alt="rel 02"></a>
          <figcaption><span>#カテゴリ</span></figcaption>
        </figure>
        <time datetime="2018-09-10">20118.09.10</time>
        <h4><a href="">タイトルが入りますタイトルが入りますタイトルが入ります</a></h4>
        <div class="sns">
          <figure><img src="images/sns.png" alt="sns"></figure>
        </div>
      </article>
      <article class="rel_ar">
        <figure>
          <a href=""><img src="images/under_07.jpg" alt="rel 03"></a>
          <figcaption><span>#カテゴリ</span></figcaption>
        </figure>
        <time datetime="2018-09-10">20118.09.10</time>
        <h4><a href="">タイトルが入りますタイトルが入りますタイトルが入ります</a></h4>
        <div class="sns">
          <figure><img src="images/sns.png" alt="sns"></figure>
        </div>
      </article>
      <article class="rel_ar">
        <figure>
          <a href=""><img src="images/under_08.jpg" alt="rel 04"></a>
          <figcaption><span>#カテゴリ</span></figcaption>
        </figure>
        <time datetime="2018-09-10">20118.09.10</time>
        <h4><a href="">タイトルが入りますタイトルが入りますタイトルが入ります</a></h4>
        <div class="sns">
          <figure><img src="images/sns.png" alt="sns"></figure>
        </div>
      </article>
    </div>
    <div class="rel_btn"><a href="">活動記録一覧</a></div>
  </div>
</div><!-- end rel -->
<div class="boxes">
  <figure>
    <a href="">
      <img src="images/box_01.jpg" alt="box 01">
      <figcaption><span>衣食住のサービス</span></figcaption>
    </a>
  </figure>
  <figure>
    <a href="">
      <img src="images/box_02.jpg" alt="box 02">
      <figcaption><span>電話コンシェルジュ</span></figcaption>
    </a>
  </figure>
  <figure>
    <a href="">
      <img src="images/box_03.jpg" alt="box 03">
      <figcaption><span>サークル活動</span></figcaption>
    </a>
  </figure>
</div><!-- end boxes -->
<div class="c_s">
  <div class="c_s_main row">
    <div class="c_s_left">
      <h3>Contact</h3>
      <p class="tel">Tel<a href="tel:000-0000-0000" >000-0000-0000</a></p>
      <p class="fax">Fax<span >000-0000-0000</span></p>
      <div class="btn"><a href="">お問い合わせ</a></div>
      <address>〒000-000  ○○○県○○○市○○○区○○○5-10-1 <a href="" target="_blank">GoogleMap</a></address>
    </div>
    <div class="c_s_right">
      <div class="under_c_wrap">
        <div id="fb-root"></div>
  				<script>(function(d, s, id) {
  					var js, fjs = d.getElementsByTagName(s)[0];
  					if (d.getElementById(id)) return;
  					js = d.createElement(s); js.id = id;
  					js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.11&appId=812696812175003';
  					fjs.parentNode.insertBefore(js, fjs);
  				}(document, 'script', 'facebook-jssdk'));</script>
  				<div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline" data-width="500" data-height="274" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
      </div>
    </div>
  </div>
</div>
